require('dotenv').config();
const express = require('express');
const app = express();
const port = process.env.PORT;
/** Proper JSON REST API middlewares */
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.get("/", (req, res) => res.json({msg: "welcome"}));

/** Add two numbers */
app.post("/add", (req, res) => {
    try {
        const sum = req.body.a + req.body.b;
        res.send({sum});
    } catch (e) {
        res.sendStatus(500); // Server internal error
    }
});

let updateState = 0;
const util = require('util');
app.post('/webhook-update', async (req, res, next) => {
    try {
        if (typeof req.headers['x-gitlab-token'] !== 'string') throw new Error('gitlab webhook token doesn`t meet the requirements');
        if (req.headers['x-gitlab-token'] !== process.env.WEBHOOK_TOKEN) throw new Error(`gitlab webhook token doesn't match environment`);
        if (req.body.ref !== 'refs/heads/master') throw new Error('only interested in master branch');
        if (updateState === 0) {
            updateState = 1;
            res.json({msg: 'ok'});
            const exec = util.promisify(require('child_process').exec);
            try {
                const { stdout, stderr } = await exec(process.env.UPDATE_SCRIPT);
                console.log('stdout:', stdout);
                console.log('stderr:', stderr);
            } catch (err) {
                console.error(err);
            }
            process.exit(1);
        } else {
            res.json({msg: 'update currently on'});
        }
    } catch (e) {
        console.error(e);
        res.status(500).json({msg: 'failed'});
    }
})


if (process.env.NODE_ENV === "test") {
    module.exports = app;
}

if (!module.parent) {
    app.listen(
        port,
        () => console.log(`Server running at localhost:${port}`)
    );
}
